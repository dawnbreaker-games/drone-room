#if UNITY_EDITOR
using UnityEngine;

namespace DroneRoom
{
	public class MakeMeshAsset : MakeAsset
	{
		public MeshFilter meshFilter;
        public bool useSharedMesh;

		public override void Do ()
		{
            if (meshFilter == null)
                meshFilter = GetComponent<MeshFilter>();
            Mesh mesh = meshFilter.mesh;
            if (useSharedMesh)
                mesh = meshFilter.sharedMesh;
			_Do (mesh, assetPath);
		}
	}
}
#else
namespace DroneRoom
{
	public class MakeMeshAsset : EditorScript
	{
	}
}
#endif