using Extensions;
using UnityEngine;

namespace DroneRoom
{
	public class Player : SingletonUpdateWhileEnabled<Player>
	{
		public Transform trs;
		public Rigidbody rigid;
		public Transform groundCheckTrs;
		public float runSpeed;
		public float jumpSpeed;
		public float flySpeed;
		public float flyDuration;
		public float maxFlyDuration;
		public float flyDurationRegenRate;
		public float flyDurationRegenDelay;
		public LayerMask whatIsWall;
		public LayerMask whatKillsMe;
		public float stopDistance;
		float timeTillFlyDurationRegen;

		public override void DoUpdate ()
		{
			if (timeTillFlyDurationRegen <= 0)
				flyDuration = Mathf.Clamp(flyDuration + flyDurationRegenRate * Time.deltaTime, 0, maxFlyDuration);
			else
				timeTillFlyDurationRegen -= Time.deltaTime;
			if (VRCameraRig.instance.leftHand.triggerInput)
				MoveToward (VRCameraRig.instance.leftHand.trs.position);
			else if (VRCameraRig.instance.rightHand.triggerInput)
				MoveToward (VRCameraRig.instance.rightHand.trs.position);
			else
			{
				Vector3 localVelocity = trs.InverseTransformDirection(rigid.velocity);
				localVelocity = Vector3.zero.SetY(localVelocity.y);
				rigid.velocity = Vector3.zero;
				rigid.AddRelativeForce(localVelocity, ForceMode.VelocityChange);
			}
		}

		void MoveToward (Vector3 position)
		{
			Vector3 desiriedMove = position - trs.position;
			Vector3 desiriedLocalMove = trs.InverseTransformDirection(desiriedMove);
			Vector3 localMove = desiriedLocalMove.normalized * 99999;
			if (Physics.Raycast(groundCheckTrs.position + trs.up * Physics.defaultContactOffset, -trs.up, Physics.defaultContactOffset * 2, whatIsWall))
			{
				Vector3 groundMovement = localMove.SetY(0);
				groundMovement = Vector3.ClampMagnitude(groundMovement, runSpeed);
				if (localMove.y > 0)
					localMove = groundMovement.SetY(jumpSpeed);
				else
					localMove = groundMovement.SetY(trs.InverseTransformDirection(rigid.velocity).y);
			}
			else
			{
				if (flyDuration > 0)
				{
					Vector3 groundMovement = localMove.SetY(0);
					groundMovement = Vector3.ClampMagnitude(groundMovement, runSpeed);
					localMove = groundMovement.SetY(trs.InverseTransformDirection(rigid.velocity).y);
					if (desiriedLocalMove.y > 0)
					{
						flyDuration -= Time.deltaTime;
						timeTillFlyDurationRegen = flyDurationRegenDelay;
						localMove.y += flySpeed * Time.deltaTime;
					}
				}
				else
					localMove = trs.InverseTransformDirection(rigid.velocity);
			}
			Vector3 move = trs.TransformDirection(localMove);
			if (Physics.Raycast(trs.position, move, move.magnitude, whatKillsMe))
				Death ();
			rigid.velocity = Vector3.zero;
			if (desiriedLocalMove.SetY(0).magnitude <= stopDistance * Level.instance.trs.lossyScale.x)
			{
				trs.localPosition += desiriedLocalMove.SetY(0);
				localMove = Vector3.zero.SetY(localMove.y);
			}
			rigid.AddRelativeForce(localMove, ForceMode.VelocityChange);
		}

		void Death ()
		{
			_SceneManager.instance.RestartSceneWithoutTransition ();
		}
	}
}