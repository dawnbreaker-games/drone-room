﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DroneRoom
{
	public class Bullet : Hazard
	{
		public float range;
		public Rigidbody rigid;
		public float moveSpeed;
		public AutoDespawnMode autoDespawnMode;
		public ObjectPool.RangedDespawn rangedDespawn;
		public new Collider collider;
		[HideInInspector]
		public bool dead;
		
		public virtual void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (rigid == null)
					rigid = GetComponent<Rigidbody>();
				if (collider == null)
					collider = GetComponentInChildren<Collider>();
				return;
			}
#endif
			dead = false;
			if (autoDespawnMode == AutoDespawnMode.RangedAutoDespawn)
				rangedDespawn = ObjectPool.instance.RangeDespawn(prefabIndex, gameObject, trs, range);
			rigid.velocity = trs.forward * moveSpeed;
		}

		public virtual void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			StopAllCoroutines();
			if (ObjectPool.instance != null && autoDespawnMode == AutoDespawnMode.RangedAutoDespawn)
				ObjectPool.instance.CancelRangedDespawn (rangedDespawn);
			dead = true;
		}

		public override void OnCollisionEnter (Collision coll)
		{
			if (!dead)
			{
				base.OnCollisionEnter (coll);
				ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
			}
		}

		public enum AutoDespawnMode
		{
			DontAutoDespawn,
			RangedAutoDespawn
		}
	}
}