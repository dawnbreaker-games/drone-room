﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DroneRoom
{
	[RequireComponent(typeof(Light))]
	public class _Light : UpdateWhileEnabled
	{
		float range;
		float intensity;
		public Transform trs;
		public new Light light;
		
		void Start ()
		{
			range = light.range;
			intensity = light.intensity;
		}
		
		public override void DoUpdate ()
		{
			light.range = range * trs.lossyScale.x;
			light.intensity = intensity * trs.lossyScale.x * trs.lossyScale.x;
		}
	}
}