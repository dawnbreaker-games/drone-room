using System;
using UnityEngine;
using System.Collections;

namespace DroneRoom
{
	[Serializable]
	public class BulletPatternEntryDontCollideWithShooter : BulletPatternEntry
	{
		public Enemy shooter;

		public override Bullet[] Shoot ()
		{
			Bullet[] output = base.Shoot();
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				for (int i2 = 0; i2 < shooter.colliders.Length; i2 ++)
				{
					Collider collider = shooter.colliders[i2];
					Physics.IgnoreCollision(bullet.collider, collider, true);
				}
				bullet.collider.enabled = true;
			}
			return output;
		}
	}
}