﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace DroneRoom
{
	[CreateAssetMenu]
	public class AimWhereFacingWithRandomOffset : AimWhereFacing
	{
		public FloatRange randomShootOffsetRange;
		
		public override Vector3 GetShootDirection (Transform spawner)
		{
			return VectorExtensions.Rotate(base.GetShootDirection(spawner), randomShootOffsetRange.Get(Random.value));
		}
	}
}