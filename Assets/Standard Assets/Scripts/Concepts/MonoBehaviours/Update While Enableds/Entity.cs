using UnityEngine;
using Pathfinding;
using Extensions;
using System.Collections.Generic;

namespace DroneRoom
{
	[ExecuteInEditMode]
	public class Entity : UpdateWhileEnabled, IDestructable
	{
		[HideInInspector]
		public float hp;
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public int maxHp;
		public int MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		[HideInInspector]
		public bool dead;
		public Transform trs;
		public Rigidbody rigid;
		public CharacterController controller;
		public float moveSpeed;
		public float jumpSpeed;
		public float maxJumpDuration;
		public float groundCheckDistance;
		public LayerMask whatICollideWith;
		public Transform groundCheckPoint;
		public float slideRate;
		public Seeker seeker;
		[Tooltip("targetDistanceFromPathPoints")]
		public float targetDistanceFromPathPoints;
		public Animator animator;
		public AnimationEntry[] animationEntries = new AnimationEntry[0];
		public Dictionary<string, AnimationEntry> animationEntriesDict = new Dictionary<string, AnimationEntry>();
		public delegate void OnDeath ();
		public event OnDeath onDeath;
		public AudioClip[] deathAudioClips = new AudioClip[0];
		protected Vector3 move;
		protected bool canJump;
		protected bool isGrounded;
		protected float timeLastGrounded;
		protected float yVel;
		protected Vector3 currentPathPoint;
		List<Vector3> pathPoints = new List<Vector3>();
		float targetDistanceFromPathPointsSqr;

		public virtual void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (trs == null)
					trs = GetComponent<Transform>();
				if (rigid == null)
					rigid = GetComponent<Rigidbody>();
				if (controller == null)
					controller = GetComponent<CharacterController>();
				if (groundCheckPoint == null)
					groundCheckPoint = trs.Find("Ground Check Point");
				if (seeker == null)
					seeker = GetComponent<Seeker>();
				return;
			}
#endif
			targetDistanceFromPathPointsSqr = targetDistanceFromPathPoints * targetDistanceFromPathPoints;
			for (int i = 0; i < animationEntries.Length; i ++)
			{
				AnimationEntry animationEntry = animationEntries[i];
				animationEntriesDict.Add(animationEntry.animatorStateName, animationEntry);
			}
		}
		
		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			if (controller != null)
			{
				move = Vector3.zero;
				isGrounded = controller.isGrounded;
				if (isGrounded)
				{
					timeLastGrounded = Time.time;
					canJump = true;
					yVel = 0;
				}
			}
			HandlePathfinding ();
			HandleRotating ();
			HandleMoving ();
			HandleGravity ();
			HandleJump ();
			if (controller != null && controller.enabled)
				controller.Move(move * Time.deltaTime);
		}

		public virtual void HandlePathfinding ()
		{
			if (pathPoints.Count > 1 && (trs.position - currentPathPoint).sqrMagnitude <= targetDistanceFromPathPointsSqr)
			{
				pathPoints.RemoveAt(0);
				currentPathPoint = pathPoints[0];
			}
		}

		public void SetDestination (Vector3 position)
		{
			seeker.CancelCurrentPathRequest();
			seeker.StartPath(trs.position, position, OnPathCalculated);
		}

		void OnPathCalculated (Path path)
		{
			pathPoints = path.vectorPath;
			currentPathPoint = pathPoints[0];
		}

		public virtual void HandleRotating ()
		{
			if (pathPoints.Count > 0)
				trs.forward = (currentPathPoint - trs.position).GetXZ();
		}
		
		public virtual void HandleMoving ()
		{
			if (pathPoints.Count > 0)
				move = (currentPathPoint - trs.position).normalized;
			if (controller != null && controller.enabled)
				move *= moveSpeed;
			else
				rigid.velocity = (move * moveSpeed).SetY(rigid.velocity.y);
		}
		
		public virtual void HandleGravity ()
		{
			if (controller != null && controller.enabled && !isGrounded)
			{
				yVel += Physics.gravity.y * Time.deltaTime;
				move += Vector3.up * yVel;
			}
		}
		
		public virtual void HandleJump ()
		{
		}
		
		protected void Jump ()
		{
			if (controller.enabled)
			{
				yVel += jumpSpeed * Time.deltaTime;
				move += Vector3.up * yVel;
			}
		}

		void OnCollisionEnter (Collision coll)
		{
			if (controller != null)
				HandleSlopes ();
		}

		void OnCollisionStay (Collision coll)
		{
			if (controller != null)
				HandleSlopes ();
		}

		void HandleSlopes ()
		{
			RaycastHit hit;
			if (Physics.Raycast(groundCheckPoint.position, Vector3.down, out hit, groundCheckDistance, whatICollideWith))
			{
				float slopeAngle = Vector3.Angle(hit.normal, Vector3.up);
				if (slopeAngle <= controller.slopeLimit)
				{
					controller.enabled = true;
					rigid.useGravity = false;
					rigid.velocity = Vector2.zero;
				}
				else
				{
					if (controller.enabled)
						rigid.velocity = controller.velocity;
					controller.enabled = false;
					rigid.useGravity = true;
					rigid.velocity += Vector3.down * Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * slideRate * Time.deltaTime;
				}
			}
		}

		public void TakeDamage (float amount)
		{
			if (dead)
				return;
			hp = Mathf.Clamp(hp - amount, 0, MaxHp);
			if (hp == 0)
			{
				dead = true;
				Death ();
			}
		}

		public void Death ()
		{
			Destroy(gameObject);
			// PlayAnimationEntry ("Death");
			// AudioManager.instance.MakeSoundEffect (deathAudioClips[Random.Range(0, deathAudioClips.Length)], trs.position);
			if (onDeath != null)
			{
				onDeath ();
				onDeath = null;
			}
		}

		public override void OnDestroy ()
		{
			base.OnDestroy ();
			onDeath = null;
		}

		public void PlayAnimationEntry (string name)
		{
			animationEntriesDict[name].Play ();
		}
	}
}