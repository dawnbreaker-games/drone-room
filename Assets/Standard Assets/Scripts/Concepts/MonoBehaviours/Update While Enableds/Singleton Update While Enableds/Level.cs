using UnityEngine;

namespace DroneRoom
{
	public class Level : SingletonUpdateWhileEnabled<Level>
	{
		public Transform trs;
		public float gravity;
		public float defaultContactOffset;

		public override void DoUpdate ()
		{
			VRCameraRig.Hand leftHand = VRCameraRig.instance.leftHand;
			VRCameraRig.Hand rightHand = VRCameraRig.instance.rightHand;
			if (leftHand.gripInput)
			{
				if (rightHand.gripInput)
					trs.SetParent(VRCameraRig.instance.bothHandsAverageTrs);
				else
					trs.SetParent(leftHand.trs);
			}
			else if (rightHand.gripInput)
				trs.SetParent(rightHand.trs);
			else
				trs.SetParent(null);
			Physics.gravity = -trs.up * gravity / trs.lossyScale.x;
			Physics.defaultContactOffset = defaultContactOffset * trs.lossyScale.x;
		}
	}
}